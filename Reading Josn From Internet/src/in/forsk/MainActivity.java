package in.forsk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private final static String TAG = MainActivity.class.getSimpleName();
	private Context context;
	String response = "";

	TextView tv;
	Button btnDownload;
	EditText etUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		context = this;

		tv = (TextView) findViewById(R.id.textView1);
		btnDownload = (Button) findViewById(R.id.button1);
		etUrl = (EditText) findViewById(R.id.editText1);

		btnDownload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// // Making a HTTP call
				// try {
				// response =
				// openHttpConnection(etUrl.getText().toString().trim());
				// Log.d(TAG, "Calling HTTP Request");
				// } catch (IOException e) {
				// e.printStackTrace();
				// Log.e(TAG, e.getMessage());
				// }
				//
				// if (BuildConfig.DEBUG) {
				// Log.d(TAG, "Url Response :- " + response);
				// }
				//
				// tv.setText(response);

				// 2222222222222222222222222222222222222222222222222222222
				// new Thread(new Runnable() {
				//
				// @Override
				// public void run() {
				// // Making a HTTP call
				// try {
				// response =
				// openHttpConnection(etUrl.getText().toString().trim());
				// Log.d(TAG, "Calling HTTP Request");
				// } catch (IOException e) {
				// e.printStackTrace();
				// Log.e(TAG, e.getMessage());
				// }
				//
				// if (BuildConfig.DEBUG) {
				// Log.d(TAG, "Url Response :- " + response);
				// }
				// // tv.setText(response);
				// 333333333333333333333333333333333333333333333333333333333333
				// runOnUiThread(new Runnable() {
				//
				// @Override
				// public void run() {
				// tv.setText(response);
				// }
				// });
				//
				// }
				// }).start();
				// 444444444444444444444444444444444444444444444444444444444444444
				new CustomAsyncTask().execute();
			}
		});

	}

	private String openHttpConnection(String urlStr) throws IOException {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertStreamToString(in);
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	// Params, Progress, Result
	class CustomAsyncTask extends AsyncTask<Void, Void, String> {

		String url;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			url = etUrl.getText().toString().trim();
			
			Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String response = "";
			try {
				response = openHttpConnection(url);
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			tv.setText(result);
			
			Toast.makeText(context, "onPostExecute", Toast.LENGTH_SHORT).show();
		}
		
		
	}
}
